'use strict';
module.exports = function(sequelize, Sequelize) {

    var Department = sequelize.define('Department', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        department_name: {
            type: Sequelize.STRING,
            allowNull: true
        },

    });

    Department.associate = function(models) {
        models.Department.hasMany(models.User, {
            as: 'users',
            through: 'UserDepartments',
            foreignKey: 'department_id'
        });
        models.Department.hasMany(models.Roles, {
            as: 'roles',
            through: 'RoleDepartments',
            foreignKey: 'department_id'
        });

    };

    return Department;
};
