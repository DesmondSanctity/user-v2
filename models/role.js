'use strict';
module.exports = function(sequelize, Sequelize) {

    var Role = sequelize.define('Role', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        role_name: {
            type: Sequelize.STRING,
            allowNull: true
        },

    });

    Role.associate = function(models) {
        models.Role.hasMany(models.User);
        models.Role.belongsToMany(models.Department, {
            as: 'departments',
            through: 'RoleDepartments',
            foreignKey: 'role_id'
        });
        models.Role.belongsTo(models.Profile, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    };


    return Role;
};
