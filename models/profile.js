'use strict';
module.exports = function(sequelize, Sequelize) {

    var Profile = sequelize.define('Profile', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        profile_name: {
            type: Sequelize.STRING,
            allowNull: true
        },

    });

    Profile.associate = function(models) {
        models.Profile.belongsToMany(models.User, {
            as: 'users',
            through: 'UserProfiles',
            foreignKey: 'profile_id'
        });

    };

    return Profile;
};
