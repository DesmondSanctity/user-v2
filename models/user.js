/**
 * Database user model.
 * Author: Babatope Olajide.
 * Version: 1.0.0
 * Release Date: 08-April-2020
 * Last Updated: 09-April-2020
 */

/**
 * Module dependencies.
 */

// put your model dependencies here...
// i.e. var moment = require('moment'); // For date handling e.t.c
// var dbLayer = require('../modules/dbLayer');

/**
 * Model Development
 */
'use strict';
module.exports = function(sequelize, Sequelize) {

    var User = sequelize.define('User', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        first_name: {
            type: Sequelize.STRING,
            allowNull: true
        },

        last_name: {
            type: Sequelize.STRING,
            allowNull: true
        },

        username: {
            type: Sequelize.TEXT,
            allowNull: false,
            validate: {
                len: [8, 50] // must be between 8 and 50.
            }
        },

        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },

        password: {
            type: Sequelize.STRING,
            allowNull: false
        },

        last_login: {
            type: Sequelize.DATE,
            allowNull: false
        },

        status: {
            type: Sequelize.ENUM('active', 'inactive'),
            defaultValue: 'active'
        },

        name: {
            type: Sequelize.STRING,
            unique: false
        },
        module_name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        module_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            validate: {
                isNumeric: true
            }
        },
        account_id: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false,
            validate: {
                isNumeric: true
            }
        },
        permission: {
            type: Sequelize.ENUM('Read and Write', 'Read only'),
            defaultValue: 'Read Only',
            allowNull: false
        },
        profile: {
            type: Sequelize.STRING,
            allowNull: false
        },
        role: {
            type: Sequelize.STRING,
            allowNull: true
        },
        department_name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        current_business: {
            type: Sequelize.STRING,
            allowNull: true
        }

    });

    User.associate = function(models) {
        models.User.belongsTo(models.Role, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        models.User.belongsTo(models.Department, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        models.User.hasMany(models.Post, {
            as: 'posts',
            through: 'UserPosts',
            foreignKey: 'user_id'
        });

        models.User.hasMany(models.currentBusiness);
    };

    return User;

};
