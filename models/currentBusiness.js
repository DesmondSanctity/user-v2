'use strict';
module.exports = function(sequelize, Sequelize) {

    var Business = sequelize.define('Business', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        currentBusiness_name: {
            type: Sequelize.STRING,
            allowNull: true
        },

        otherBusinesses: {
            type: Sequelize.STRING,
            allowNull: true
        }
    });

    Business.associate = function(models) {
        models.Business.hasMany(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Business;
};
