'use strict';
module.exports = (sequelize, DataTypes) => {
    var Post = sequelize.define('Post', {
        post_title: DataTypes.STRING,
        poste_body: DataTypes.TEXT,
        UserId: DataTypes.INTEGER
    });
    // create post association
    // a post will have an author
    // a field called AuthorId will be created in our post table inside the db
    Post.associate = function(models) {
        models.Post.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    };




    return Post;
};
